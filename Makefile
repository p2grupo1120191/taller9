pscopy: obj/main.o
	gcc obj/main.o -o bin/pscopy

obj/main.o: src/main.c
	gcc -Wall -c -I include/ src/main.c -o obj/main.o

.PHONY: clean
clean:
	rm bin/* obj/*
