/*Programacion Sistemas Grupo#11
	Taller#9
Integrantes:
	-Ariana Ochoa Ochoa
	-Erick Nunez Salazar*/
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>

size_t copia(int old, int new);

int main(int argc, char **argv){
	int fd, fd2;
	size_t cantidad;
	umask(0);
	if (argc!=3){
		fprintf(stderr,"Error: Se precisan 2 argumentos\n");
		exit(1);
	}

	fd=open(argv[1], O_RDONLY);
	
	if (fd==-1){
		fprintf(stderr, "Error al abrir el fichero %s: No such file or directory\n",
		argv[1]);
		exit(1);
	}

	fd2=open(argv[2], O_CREAT| O_WRONLY | O_TRUNC,0666);
	
	if (fd2 == -1){
		fprintf(stderr, "Error al abrir o crear el fichero %s\n",
		argv[2]);
		exit(1);
	}
	
	cantidad = copia(fd, fd2);
	close(fd);
	close(fd2);
	printf("%ld bytes copiados.\n",cantidad);

	return 0;
}


size_t copia(int viejo, int nuevo){

	size_t bytes_leidos;
	size_t total= 0;
	char buffer[20000];
	while ((bytes_leidos=read(viejo, buffer, sizeof(buffer)))>0){
		write(nuevo, buffer, bytes_leidos);
		total+= bytes_leidos;
	}
	return total;
}
